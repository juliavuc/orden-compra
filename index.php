<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <title>Orden de compra</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="assets/css/estilos.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2? family = Roboto: ital, wght @ 0,100; 0,300; 0,400; 0,500; 0,700; 0,900; 1,300; 1,400; 1,500; 1,900 & display = swap " rel=" stylesheet ">
</head>

<body>
    <main>

        <div class="contenedor__todo">
            
            <div class="caja__trasera">

                <div class="caja__trasera-login">
                    <h3>¿Sos vendedor/a?</h3>
                    <p>Iniciá sesión para actualizar tus productos</p>
                    <button id="btn__iniciar-sesión">Iniciar Sesión</button>
                </div>

                <div class="caja__trasera-register">
                    <h3>¿Querés realizar tu pedido?</h3>
                    <p>¡Completá tus datos acá!</p>
                    <button id="btn__ingresar-datos">Ingresar datos</button>
                </div>

            </div>
            
            <div class="contenedor__login-register">
                
                <form action="" class="formulario__login">
                    <h2>Iniciar Sesión </h2>
                    <input type="text" placeholder="Correo electrónico">
                    <input type="password" placeholder="Contraseña">
                    <button>Entrar</button>
                </form>
                
                <form action="php/registro_usuario_be.php" method="POST" class="formulario__register">
                    <h2>Ingresar datos</h2>
                    <input type="text" placeholder="Nombre" name="nombre">
                    <input type="text" placeholder="Apellido" name="apellido">
                    <input type="text" placeholder="Empresa" name="empresa">
                    <input type="text" placeholder="DNI" name="dni">
                    <input type="text" placeholder="Teléfono" name="telefono">
                    <input type="text" placeholder="Mail" name="mail">
                    <input type="text" placeholder="Dirección de la entrega" name="direccion">
                    <input type="text" placeholder="Producto" name="producto">
                    <input type="number" placeholder="Cantidad (unidades)" name="cantidad_kg">
                    <button>Enviar</button>
                </form>

            </div>
        </div>

    </main>
    <script src="assets/js/script.js"></script>
</body>

</html>